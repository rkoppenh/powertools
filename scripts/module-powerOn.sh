#!/bin/bash

#
# Enable the power on a X hybrid
#

#
# Enable the DCDC
./bin/pbv3_field write DCDCen 1
./bin/pbv3_field write DCDCenC 1

#
# Enable the Hybrid

# resetB
./bin/pbv3_field write RstCntHyHCCresetB 1
./bin/pbv3_field write RstCntCHyHCCresetB 1
./bin/pbv3_field write RstCntHxHCCresetB 1
./bin/pbv3_field write RstCntCHxHCCresetB 1

# Hybrid LDOs
./bin/pbv3_field write CntSetHyLDO0en 1
./bin/pbv3_field write CntSetCHyLDO0en 1
./bin/pbv3_field write CntSetHyLDO1en 1
./bin/pbv3_field write CntSetCHyLDO1en 1
./bin/pbv3_field write CntSetHyLDO2en 1
./bin/pbv3_field write CntSetCHyLDO2en 1

./bin/pbv3_field write CntSetHxLDO0en 1
./bin/pbv3_field write CntSetCHxLDO0en 1
./bin/pbv3_field write CntSetHxLDO1en 1
./bin/pbv3_field write CntSetCHxLDO1en 1
./bin/pbv3_field write CntSetHxLDO2en 1
./bin/pbv3_field write CntSetCHxLDO2en 1

# HV mux
./bin/pbv3_field write CntSetCHV0en 1
./bin/pbv3_field write CntSetHV0en 1


