#ifndef PBV3TBCONF_H
#define PBV3TBCONF_H

#include <EquipConf.h>

#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>

using json = nlohmann::json;

#include "PBv3TB.h"

/** \brief Factory for creating PBv3 testbenches
 *
 * Valid JSON hardware config file is made up of the following blocks:
 *  - testbench: test benches registered in a registry
 *
 * Necessary power supplies should also be defined in the same configuration
 * file. The PBv3TBConf takes care of creating an internal EquipConf factory
 * and associating PowerSupplyChannel objects to the testbenches. Currently,
 * the Vin and HVin channels are added to all testbenches.
 */
class PBv3TBConf {
 public:
    /** Configuration @{ */

    /** Constructor */
    PBv3TBConf();

    /** Constructor
     * \param hardwareConfigFile input JSON file with list of hardware resources
     * and options
     */
    PBv3TBConf(const std::string &hardwareConfigFile);

    /** Constructor
     * \param hardwareConfig JSON object with list of hardware sources and
     * options
     */
    PBv3TBConf(const json &hardwareConfig);

    ~PBv3TBConf();

    /** Set input hardware list file
     * \param hardwareConfigFile input JSON file with list of hardware resources
     * and options
     */
    void setHardwareConfig(const std::string &hardwareConfigFile);

    /** Set input hardware list file
     * \param hardwareConfig input JSON object with list of hardware resources
     * and options
     */
    void setHardwareConfig(const json &hardwareConfig);

    /** Get testbench JSON configuration
        \param label testbench name
        \return device JSON configuration (by reference)
     */
    json getPBv3TBConf(const std::string &label);

    /** @} */

 public:
    /** Get (or create) testbenches (by label)
     * @{
     */

    /** Get PBv3TB object corresponding to name
     *
     * If this is the first time the testbench is requested:
     *  1. Create the testbench
     *  2. Configure the testbench
     *  3. Initialize the testbench
     *
     * \param name label for the hardware object in JSON configuration file
     */
    std::shared_ptr<PBv3TB> getPBv3TB(const std::string &name);

    /** @} */

 private:
    //! JSON file with hardware list and options (see example input-hw.json file
    //! for syntax)
    json m_hardwareConfig;

    //! Power supply factory
    EquipConf m_hw;

    //! Stored handles of IPowerSupply pointers created
    std::unordered_map<std::string, std::shared_ptr<PBv3TB>> m_listPBv3TB;
};

#endif
