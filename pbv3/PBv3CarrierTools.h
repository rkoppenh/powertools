#ifndef PBV3CARRIERTOOLS_H
#define PBV3CARRIERTOOLS_H

#include <nlohmann/json.hpp>

#include "PBv3TBMassive.h"

/**
 * \brief Module defining several tests for a plain carrier card
 *
 *  - All tests return a JSON format that can be directly
 *    uploaded to the ITk Production Database
 */
namespace PBv3CarrierTools {

//! \brief LV_ENABLE Check current with 11V on
/**
 * Pass: Input current is below 5 mA.
 *
 * \param tb Testbench object
 *
 * \return json object with test results
 */
nlohmann::json testLvEnable(std::shared_ptr<PBv3TBMassive> tb);

//!  \brief MUXCOM Test communication with multiplexers
/**
 * This really test communication with the IO expander used to
 * select the multiplexers.
 *
 * Pass: Toggling of all 10 multiplexers works and all return <50 mV
 *
 * \param tb Testbench object
 *
 * \return json object with test results
 */
nlohmann::json testMuxes(std::shared_ptr<PBv3TBMassive> tb);

}  // namespace PBv3CarrierTools

#endif  // PBV3CARRIERTOOLS_H
