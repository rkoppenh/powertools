#ifndef AMACV2REGISTER_H
#define AMACV2REGISTER_H

#include <iostream>

enum rw_t { RO = 1, WO = 2, RW = 3 };

//! \brief Description of an AMACv2 register
class AMACv2Register {
 public:
    //! Create an undefined (invalid) register
    AMACv2Register() = default;

    //! Create a named register
    /**
     * Default register values should be set through field
     * operations.
     *
     * \param name Register name
     * \param address Register address
     * \param rw Read/write definition
     */
    AMACv2Register(const std::string &name, uint32_t address, rw_t rw);

    //! Register defintion is valid
    bool isValid() const;

    //! Get the name
    std::string getName() const;

    //! Get address
    uint8_t getAddress() const;

    //! set value
    void setValue(uint32_t value);

    //! Get value
    uint32_t getValue() const;

    //! Get RW definition
    rw_t isRW() const;

 private:
    //! Helpful name for register
    std::string m_name = "";

    //! Register address
    uint8_t m_address = 255;
    //! Register value
    uint32_t m_value = 0;

    //! Read/write mode
    rw_t m_rw = RO;
};

#endif  // AMACV2REGISTER_H
