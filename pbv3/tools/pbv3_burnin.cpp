#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <sstream>
#include <thread>

#include "AMACv2.h"
#include "EndeavourComException.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3QCFlow.h"
#include "PBv3TBConf.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"
#include "PowerSupplyChannel.h"

//------ SETTINGS
#ifdef FTDI
std::vector<uint32_t> pbNum = {0};
std::vector<std::string> serial = {};
#else
std::vector<uint32_t> pbNum = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
#endif  // FTDI

std::string equipConfigFile = "config/equip_testbench.json";
bool skipSmoke = false;
float load = 1.0;      // Amp
float burntime = 1.0;  // minutes

const float PMax_PS = 48.0;
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] panelNum" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -b, --board             Powerboard numbers to test, separated "
           "by commas. (default: 0 if single testbench, all 0,..,9 if mass "
           "testbench)"
        << std::endl;
    std::cerr << " -l, --load              output current (default: " << load
              << " A)" << std::endl;
    std::cerr << " -t, --burntime              running time (default: "
              << burntime << " minutes)" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << "     --skipsmoke         Skip inital power-up current check"
              << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << " -h, --help              Print this help text" << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    //
    // Parse input options to configure test
    //
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"load", required_argument, 0, 'l'},
            {"burntime", required_argument, 0, 't'},
            {"equip", required_argument, 0, 'e'},
            {"skipsmoke", no_argument, 0, 1},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:t:l:e:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 1:
                skipSmoke = true;
                break;
            case 'b': {
                pbNum.clear();
                std::stringstream pb_stream(optarg);
                std::string pb_substr;
                while (getline(pb_stream, pb_substr, ',')) {
                    pbNum.push_back(stoi(pb_substr));
                }
            } break;
            case 'l':
                load = std::stof(optarg);
                break;
            case 't':
                burntime = std::stof(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (argc - optind < 0) {
        std::cerr << "Required paths missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    //
    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);

    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    // Power up the testbench
    tb->powerTBOn();

    // Make sure to start in off state
    logger(logINFO) << "Turn off PS";
    tb->powerLVOff();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // Turn on power
    logger(logINFO) << "Turn on LV fully";
    tb->powerLVOn(4.8);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    float poweroncurrent = tb->getVinCurrent();
    logger(logINFO) << "LV current: " << poweroncurrent << "A";
    if (!skipSmoke && poweroncurrent > 0.7) {
        tb->powerLVOff();
        logger(logERROR) << "Power-up current too high. Stopping test..";
        return 1;
    }

    logger(logINFO) << "Turning on HVPS";
    std::shared_ptr<PowerSupplyChannel> hv = tb->getHVPS();
    hv->turnOff();
    hv->setVoltageLevel(0);
    hv->setCurrentProtect(2e-3);
    hv->turnOn();
    hv->rampVoltageLevel(-500, 40);
    std::this_thread::sleep_for(std::chrono::seconds(2));

    std::vector<std::shared_ptr<AMACv2>> amacs;

    logger(logINFO) << "Start burn in at "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    // Start burn in!
    float load_initial = 1.0;
    if (load < load_initial) load_initial = load;

    for (uint32_t i = 0; i < pbNum.size(); i++) {
        logger(logINFO) << "Init AMAC on PB " << pbNum[i];
        std::shared_ptr<AMACv2> amac = tb->getPB(pbNum[i]);
        try {
            amac->init();
            json defconfig;
            PBv3ConfigTools::configAMAC(amac, defconfig, false);
            amac->initRegisters();
            PBv3ConfigTools::saveConfigAMAC(amac, defconfig);
        } catch (EndeavourComException &e) {
            logger(logERROR) << "Unable to initialize AMACv2 on " << pbNum[i];
            logger(logERROR) << e.what();
            continue;
        }

        logger(logINFO) << "Turn on DCDC on PB " << pbNum[i];
        amac->wrField(&AMACv2::DCDCen, 1);
        amac->wrField(&AMACv2::DCDCenC, 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        logger(logINFO) << "Turn on load to " << load_initial << "A on PB "
                        << pbNum[i];
        tb->setLoad(pbNum[i], load_initial);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        double Iout = tb->getIload(pbNum[i]);
        logger(logINFO) << "Measured load current: " << Iout << "A on PB "
                        << pbNum[i];

        // HVmux off
        logger(logINFO) << "Disabling CntSetHV0en";
        amac->wrField(&AMACv2::CntSetHV0en, 0);
        amac->wrField(&AMACv2::CntSetCHV0en, 0);
        std::this_thread::sleep_for(std::chrono::seconds(2));
        amacs.push_back(amac);
    }

    // now slowly increase the load until the PS input power reaches PMax_PS
    float load_step = 0.05;
    int n_steps = int((load - load_initial) / load_step);

    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    double Vin = tb->getVin();
    double Iin = tb->getVinCurrent();
    for (int istep = 0; istep < n_steps; istep++) {
        if (PMax_PS - Iin * Vin < 2.0 * 10 * 1.5 * load_step)
            break;  // allow one more increase
        float load_now = load_initial + (istep + 1) * load_step;
        logger(logINFO) << "Increase load to " << load_now << " A per PB";
        for (uint32_t i = 0; i < pbNum.size(); i++) {
            tb->setLoad(pbNum[i], load_now);
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        Iin = tb->getVinCurrent();
        logger(logINFO) << "Input current  = " << Iin
                        << " A, power = " << Iin * Vin << " W";
    }

    auto start = std::chrono::high_resolution_clock::now();

    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    Iin = tb->getVinCurrent();
    double hv_v_off = fabs(hv->measureVoltage());
    double hv_i_off = fabs(hv->measureCurrent());

    logger(logINFO) << "Input voltage: LV = " << Vin << "V; HV = " << hv_v_off
                    << "V";
    logger(logINFO) << "Input current: LV = " << Iin << "A; HV = " << hv_i_off
                    << "A";

    int burn_step = int(burntime * 60 / 10);   // in seconds
    if (burn_step > 600.0) burn_step = 600.0;  // report every 10 minutes
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;

    while (elapsed.count() / 60.0 < burntime) {
        std::this_thread::sleep_for(std::chrono::seconds(burn_step));
        finish = std::chrono::high_resolution_clock::now();
        elapsed = finish - start;
        Iin = tb->getVinCurrent();
        hv_i_off = fabs(hv->measureCurrent());

        logger(logINFO) << "Time elapsed: " << elapsed.count() / 60.0 << " m ("
                        << elapsed.count() / 3600.0 << " h), time remaining: "
                        << burntime - elapsed.count() / 60.0 << " m ("
                        << burntime / 60.0 - elapsed.count() / 3600.0 << " h), "
                        << "LV input current: " << Iin
                        << " A, HV input current: " << hv_i_off << " A";
    }

    logger(logINFO) << "Stop burn in at "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    logger(logINFO) << "Time elapsed: " << elapsed.count() / 60.0 << " m ("
                    << elapsed.count() / 3600.0 << " h)";

    logger(logINFO) << "Disable DCDC";
    for (uint32_t i = 0; i < amacs.size(); i++) {
        amacs[i]->wrField(&AMACv2::DCDCen, 0);
        amacs[i]->wrField(&AMACv2::DCDCenC, 0);
    }

    logger(logINFO) << "Turn OFF load";
    for (uint32_t i = 0; i < pbNum.size(); i++) {
        tb->loadOff(pbNum[i]);
    }

    logger(logINFO) << "Power off";

    tb->powerLVOff();
    hv->rampVoltageLevel(0, 40);
    hv->turnOff();

    tb->powerTBOff();

    return 0;
}
