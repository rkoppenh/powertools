#include "AMACv2.h"
#ifdef FTDI
#include "EndeavourRawFTDI.h"
#else
#include "EndeavourRawUIO.h"
#endif  // FTDI
#include <unistd.h>

#include <iomanip>
#include <iostream>
#include <memory>
#include <vector>

#include "ComIOException.h"
#include "EndeavourComException.h"
#include "UIOCom.h"

// Option parsing
uint32_t amacid = 0;
uint32_t trails = 1000;
#ifndef FTDI
std::string uiodev = "/dev/uio0";
#endif  // FTDI

void print_help() {
    std::cout << "usage: EndeavourBER [driver options]" << std::endl;
    std::cout << std::endl;
    std::cout << "driver options" << std::endl;
    std::cout << " -a amacid: amacid to use for communication (default: "
              << amacid << ")" << std::endl;
    std::cout << " -n trails: number of tests to run (default: " << trails
              << ")" << std::endl;
#ifndef FTDI
    std::cout << " -d uio: UIO device to use (default: " << uiodev << ")"
              << std::endl;
#endif  // FTID
}

int main(int argc, char *argv[]) {
    // Parse options
    int opt;
#ifdef FTDI
    while ((opt = getopt(argc, argv, "a:n:")) != -1)
#else
    while ((opt = getopt(argc, argv, "d:a:n:")) != -1)
#endif
    {
        switch (opt) {
            case 'a':
                amacid = std::stoul(optarg, nullptr, 0);
                break;
            case 'n':
                trails = std::stoul(optarg, nullptr, 0);
                break;
#ifndef FTDI
            case 'd':
                uiodev = optarg;
                break;
#endif
            default:
            case '?':  // unknown option...
                print_help();
                return 1;
                break;
        }
    }

    //
    // Run test

#ifdef FTDI
    AMACv2 amac(amacid, std::unique_ptr<EndeavourRaw>(new EndeavourRawFTDI()));
#else
    std::shared_ptr<UIOCom> uio = std::make_shared<UIOCom>(uiodev, 0x10000);
    AMACv2 amac(amacid,
                std::unique_ptr<EndeavourRaw>(new EndeavourRawUIO(uio)));
#endif  // FTDI

    uint good = 0;
    for (uint i = 0; i < trails; i++) {
        try {
            uint valin = rand() * 0xFFFFFFFF;
            amac.write_reg(166, valin);
            usleep(50);
            uint valout = amac.read_reg(166);
            usleep(50);
            if (valin == valout)
                good++;
            else
                std::cout << "Write: 0x" << std::hex << valin
                          << ", Read: " << valout << std::dec << std::endl;
        } catch (EndeavourComException &e) {
            std::cout << e.what() << std::endl;
        }
    }

    std::cout << "Reliability: " << ((float)good) / trails << std::endl;

    return 0;
}
