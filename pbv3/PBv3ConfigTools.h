#ifndef PBV3CONFIGTOOLS_H
#define PBV3CONFIGTOOLS_H

#include <DACDevice.h>

#include <chrono>
#include <iomanip>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <thread>

#include "AMACv2.h"
#include "Logger.h"
#include "PBv3TB.h"

using nlohmann::json;

/**
 * \brief Module for configuring the AMACv2 using JSON files.
 *
 * The module can be split into three functions
 *  - Applying the configuration of in a JSON file to an AMAC object (config)
 *  - Finding the optimal settings for an AMAC (tune)
 *  - Determining the AMAC response to input using fixed settings (calibrate)
 *
 * All tune/calibrate functions modify the state of the AMAC object based on the
 * result.
 *
 * All functions return a consistent configuration for the chip. For example,
 * tunning the AM bandgap changes the ADC response. Thus the result of the
 * tuneAMBG also contains updated calibration.
 *
 * The output format is compatible with the CONFIG test type in the ITk PD.
 */
namespace PBv3ConfigTools {
//
// Copy JSON configuration to AMAC

/**
 * \brief Configure an AMAC using the contents of a JSON file.
 *
 * Configuration of registers means changing the default values.
 *
 * The format of the JSON configuration assumes the following:
 *  - key "properties" points to a dictionary where the key is the field name
 * and value the value to be set
 *  - key "results" contains a dictionary with calibration values
 *
 * If the JSON contains a "config" key, then the properties and results are
 * looked for there. Other wise they are accessed from the top-level of the JSON
 * object.
 *
 * \param amac Pointer to the AMACv2 object
 * \param config JSON configuration to apply
 * \param write Write the results to the AMACv2 chip in addition to updating the
 * default values
 */
void configAMAC(std::shared_ptr<AMACv2> amac, const json &config, bool write);

/**
 * \brief Read current AMAC configuration and save it to a JSON object
 *
 * \param amac Pointer to the AMACv2 object
 * \param config JSON configuration to update
 */
void saveConfigAMAC(std::shared_ptr<AMACv2> amac, json &config);

/**
 * \brief Decorate a JSON config object with information that can be saved to a
 * database
 *
 * Adds the following fields:
 *  - testType: "CONFIG"
 *  - institution: "LBL"
 *  - runNumber: "0-0" by default, increment lower number by 1 if already exists
 *
 * \param config JSON configuration to update
 */
void decorateConfig(json &config);

//
// All helper functions for tuning the AMAC settings

//! Find the VDDbg setting for VDDREG to be 1.2V
/**
 * Also performs AM calibration as it depends on the
 * regulated voltage.
 *
 * Modifes the following registers:
 *  - VDDbg
 *  - VDDbgen
 *
 * Updates the AM calibration.
 *
 * \param amac Pointer to the AMACv2 object
 * \param CALdac Pointer to the DAC that drives the CAL line
 *
 * \return updated json configuration
 */
json tuneVDDBG(std::shared_ptr<AMACv2> amac, std::shared_ptr<DACDevice> CALdac);

//! \brief Find the AMbg setting for AM bandgap to be 600 mV
/**
 * Modifes the following registers:
 *  - AMbg
 *  - AMbgen
 *
 * Updates the AM calibration.
 *
 * \param amac Pointer to the AMACv2 object
 * \param CALdac Pointer to the DAC that drives the CAL line
 *
 * \return updated json configuration and calibration
 */
json tuneAMBG(std::shared_ptr<AMACv2> amac, std::shared_ptr<DACDevice> CALdac);

//! \brief Find the ramp gain for ADC response of 1mV / count
/**
 * Modifes the following registers:
 *  - AMintCalib
 *
 * Updates the ADC calibration.
 *
 * \param amac Pointer to the AMACv2 object
 * \param CALdac Pointer to the DAC that drives the CAL line
 *
 * \return updated json configuration and calibration
 */
json tuneRampGain(std::shared_ptr<AMACv2> amac,
                  std::shared_ptr<DACDevice> CALdac);

/**
 * \brief Tune the offset for the input current monitor block
 *
 * This procedure requires that there is no load on the input current. This
 * means that the bPOL must be disabled. If it is enabled, a warning is
 * printed and the function returns.
 *
 * Step 1: Measure the buffer offset difference
 *  Shorts the low and high connections and measures the buffered value
 *  at the low and high test points. The test points are disconnected
 *  after this step.
 *
 * Step 2:
 *  Measures the difference between the two current mirrors as a function
 *  of the DCDCiN/P register settings by comparing the test point values. The
 *  offset from step 1 is subtracted from the High-Low TP difference.

 *  The value that gives a difference closest to zero is chosen as the tuning.
 *
 * Step 3:
 *  Measures the final amplifier output with the High/Low connections shorted
 *  as a function of the DCDCiOffset value. The offset with the High/Low
 *  connections disconnected at the tuned value is also measured.
 *
 *  The value giving an offset closest to 250 mV is closen as the tuning.
 *
 * The following results are saved in the resulting JSON object:
 *  - DCDCiOffset: Offset giving the closest "zero reading" to 250 mV
 *  - DCDCiP: Low current mirror setting giving the closest match
 *  - DCDCiN: High current mirror setting giving the closest match
 *
 * The following properties are saved in the resulting JSON object:
 *  - CUR10VOFFSETZERO: Buffer offset in "zero reading" mode at `DCDCiOffset`
 *  - CUR10VCM: Current mirror mismatch at `DCDCiP`/`DCDCiN`
 *  - CUR10VOFFSET: Buffer offset mode at `DCDCiOffset` "zero" current
 *
 * Modifes the following registers:
 *  - Ch12Mux
 *  - DCDCiP
 *  - DCDCiN
 *  - DCDCiOffset
 *  - DCDCiZeroReading
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json configuration and calibration
 */
json tuneCur10V(std::shared_ptr<AMACv2> amac);

/**
 * \brief Tune the offset for the output current monitor block
 *
 * Scans through all settings for the DCDCoOffset register until it finds
 * a configuration where the ADC reads ~100 mV when the two measurements
 * shorted together.
 *
 * Also prints values for the current monitor test point values
 * when the zero calibration is disabled, but does not do anything with them.
 *
 * Modifes the following registers:
 *  - DCDCoOffset
 *
 * Updates the current offset calibration based on 100 readings.
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json configuration and calibration
 */
json tuneCur1V(std::shared_ptr<AMACv2> amac);

/**
 * \brief Find the NTC sense range setting for the current temperature
 *
 * Best setting is defined as the value that gives the reading closest
 * to 500 counts
 *
 * Modifes the following registers:
 *  - NTCx0SenseRange
 *  - NTCy0SenseRange
 *  - NTCpbSenseRange
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json configuration
 */
json tuneNTC(std::shared_ptr<AMACv2> amac);

//
// All helper functions for calibration of the AMAC

/**
 * \brief Calibrate the ADC response slope
 *
 * Updates the ADC calibration slope in mV/count.
 *
 * \param amac Pointer to the AMACv2 object
 * \param CALdac Pointer to the DAC that drives the CAL line
 *
 * \return updated json calibration
 */
json calibrateSlope(std::shared_ptr<AMACv2> amac,
                    std::shared_ptr<DACDevice> CALdac);

/**
 * \brief Calibrate the ADC response offsets
 *
 * Updates the ADC calibration offsets in counts.
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json calibration
 */
json calibrateOffset(std::shared_ptr<AMACv2> amac);

/**
 * \brief Calibrate the NTC reference voltage
 *
 * Updates the NTC reference voltages (pb,x,y) in mV.
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json calibration
 */
json calibrateNTC(std::shared_ptr<AMACv2> amac);

/**
 * \brief Calibrate the input Current Monitor offset
 *
 * Updates the offset calibration for input CM
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json calibration
 */
json calibrateCur10V(std::shared_ptr<AMACv2> amac);

/**
 * \brief Calibrate the output Current Monitor offset
 *
 * Updates the offset calibration for output CM
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json calibration
 */
json calibrateCur1V(std::shared_ptr<AMACv2> amac);

/**
 * \brief Calibrate the absolute temperature scale
 *
 * Calibrates the PTAT and CTAT by comparing the temperature
 * with the DC/DC converter in OFF state. Since there is no
 * major power usage, this assumes that the temperature in
 * this state is uniform across the board.
 *
 * If the DC/DC converter is turned on, it is disabled and
 * the program waits until PTAT stabilizes.
 *
 * For CTAT, all offset settings are scanned.
 *
 * \param amac Pointer to the AMACv2 object
 *
 * \return updated json calibration
 */
json calibrateTemperature(std::shared_ptr<AMACv2> amac);

/**
 * \brief Determine the AMAC PADID
 *
 * The implementation requires the AMAC ID to be set again even if it was not
 * previously.
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench with all control classes
 *
 * \return updated json configuration
 */
json scanPADID(uint32_t pbNum, std::shared_ptr<PBv3TB> tb);
}  // namespace PBv3ConfigTools

#endif
